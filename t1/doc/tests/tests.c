#include <stdio.h>
#include <graphviz/cgraph.h>

int main()
{
	//--------------------------------------
	// Error tests
	FILE *f = fopen("sample_graph.dot", "r");
	FILE *out = fopen("/dev/null", "w");
	Agraph_t *g = agread(f, NULL);
	
	printf("agread(NULL, NULL) causes segfault\n");
	printf("agread(out, NULL) returns %p\n", agread(out, NULL));
	printf("agread(f, NULL) returns %p\n", g);

	printf("agwrite(NULL, NULL) causes segfault\n");
	printf("agwrite(g, NULL) causes segfault\n");
	printf("agwrite(NULL, out) causes segfault\n");
	printf("agwrite(g, f) returns %d\n", agwrite(g, f));
	printf("agwrite(g, out) returns %d\n", agwrite(g, out));

	agfree(g, NULL);
	g = NULL;

	return 0;
}