#include <stdio.h>
#include <stdlib.h>
#include <graphviz/cgraph.h>
#include "grafo.h"

#define MIN_ALLOC 100

typedef Agnode_t *vertice;
typedef Agedge_t *aresta;

typedef struct weight_t {
	Agrec_t hdr;
	unsigned long w;
} weight_t;

char CONST_STR_WEIGHT[] = "weight";
char CONST_STR_WEIGHT_T[] = "weight_t";
char CONST_STR_TIPO[] = "tipo";
char CONST_STR_0[] = "0";
char CONST_STR_RECOMENDACOES[] = "recomendacoes";


//------------------------------------------------------------------------------
// (apontador para) estrutura de dados para representar um grafo
// 
// o grafo pode ser direcionado ou não
// 
// o grafo tem um nome, que é uma "string"

struct grafo {
	Agraph_t *g;
};

//------------------------------------------------------------------------------
/*
 * FUNCOES AUXILIARES INTERNAS
 */

long unsigned int tamanho(vertice *V);
int gera_recomendacoes(grafo g, grafo r, vertice c1, vertice c2);
vertice *intersecao(vertice *V1, vertice *V2);
vertice *vizinhanca(grafo g, vertice v);
vertice *diferenca(vertice *V1, vertice *V2);
vertice *busca_atributo(grafo g, char *atributo, const char *valor);

/**
 * @brief      Retorna o tamanho do conjunto de vertices V
 *
 * @param      V     Conjunto de vertices
 *
 * @return     Tamanho de V
 */
long unsigned int tamanho(vertice *V)
{
	long unsigned int i;
	for(i = 0; V[i]; i++);
	return i + 1;
}

/**
 * @brief      Gera recomendacoes de c1 para c2 do grafo g no grafo r
 *
 * @param      g     Grafo g (original)
 * @param      r     Grafo r (recomendacoes)
 * @param      c1    Vertice c1
 * @param      c2    Vertice c2
 *
 * @return     0 em caso de sucesso, -1 em caso de erro
 */
int gera_recomendacoes(grafo g, grafo r, vertice c1, vertice c2)
{
	// Loop duplo que passa pelas arestas dos consumidores, dois a dois
	vertice *produtos_c1 = vizinhanca(g, c1);
	vertice *produtos_c2 = vizinhanca(g, c2);

	if( !produtos_c1 || !produtos_c2 ) return -1;

	vertice *_intersecao = intersecao(produtos_c1, produtos_c2);
	vertice *_diferenca = diferenca(produtos_c1, produtos_c2);

	if( tamanho(_intersecao) >= tamanho(_diferenca))
	{
		// Consumidor j ganha tamanho(_diferenca) recomendacoes
		vertice vj = agnode(r->g, agnameof(c2), 0);

		vertice *recomendacoes = _diferenca; // Para coerencia
		for(int k = 0; recomendacoes[k]; k++)
		{
			// Pega o vertice-produto de recomendacao
			vertice vp = agnode(r->g, agnameof(recomendacoes[k]), 0);

			// Cria a aresta entre os dois ou pega a existente
			aresta a = agedge(r->g, vj, vp, NULL, 1);
			
			// Apontador para guardar um record de peso
			weight_t *w = (weight_t *) aggetrec (a, CONST_STR_WEIGHT_T, TRUE);

			// Se nao tem record ainda, cria um novo
			if( !w )
			{
				w = (weight_t *) agbindrec (a, CONST_STR_WEIGHT_T, sizeof(weight_t), TRUE);
				w->w = 0;
			}

			w->w++;
		}
	}

	free(produtos_c1);
	free(produtos_c2);
	free(_intersecao);
	free(_diferenca);

	return 0;
}

/**
 * @brief      Busca todos os vertices do grafo g que tenham o atributo=valor
 *             dados
 *
 * @param      g         Grafo
 * @param      atributo  Atributo
 * @param      valor     Valor do atributo
 *
 * @return     Conjunto de vertices que tenham [atributo=valor] (conjunto vazio
 *             se nao houver), ou NULL em caso de erro.
 */
vertice *busca_atributo(grafo g, char *atributo, const char *valor)
{
	vertice *V = (vertice *) calloc (MIN_ALLOC, sizeof(vertice));
	long unsigned int tam_V = 0;
	long unsigned int alloc_V = MIN_ALLOC;

	if( !V ) return NULL;

	for(vertice v = agfstnode(g->g); v; v = agnxtnode(g->g, v))
	{
		char *attr = agget(v, atributo);
		if( !strcmp(valor, attr) )
		{
			V[tam_V++] = v;

			if(tam_V == alloc_V - 1)
			{
				V = (vertice *) realloc (V, (alloc_V + MIN_ALLOC) * sizeof(vertice));

				if( !V ) return NULL;

				memset(V + alloc_V, 0, MIN_ALLOC * sizeof(vertice));
				alloc_V += MIN_ALLOC;
			}
		}
	}

	return V;
}

/**
 * @brief      Retorna o conjunto-interseção entre dois conjuntos de vertices V1
 *             e V2
 *
 * @param      V1    Conjunto de vertices V1
 * @param      V2    Conjunto de vertices V2
 *
 * @return     Conjunto-interseção entre V1 e V2, ou NULL em caso de erro.
 */
vertice *intersecao(vertice *V1, vertice *V2)
{
	vertice *intersecao = (vertice *) calloc (MIN_ALLOC, sizeof(vertice));
	long unsigned int tam_intersecao = 0;
	long unsigned int alloc_intersecao = MIN_ALLOC;

	if( !intersecao ) return NULL;

	for(long unsigned int i = 0; V1[i]; i++)
	{
		long unsigned int eh_da_intersecao = 0;

		for(long unsigned int j = 0; V2[j]; j++)
		{
			if(V1[i] == V2[j])
			{
				eh_da_intersecao = 1;
				break;
			}
		}

		if(eh_da_intersecao)
		{
			intersecao[tam_intersecao++] = V1[i];

			if( tam_intersecao == alloc_intersecao - 1 )
			{
				intersecao = (vertice *) realloc (intersecao, (alloc_intersecao + MIN_ALLOC) * sizeof(vertice));

				if( !intersecao ) return NULL;

				memset(intersecao + alloc_intersecao, 0, MIN_ALLOC * sizeof(vertice));
				alloc_intersecao += MIN_ALLOC;
			}
		}
	}

	return intersecao;
}

/**
 * @brief      Retorna a vizinhanca do vertice v do grafo g
 *
 * @param      g     Grafo que contem v
 * @param      v     Vertice
 *
 * @return     Ponteiro para vetor de vertices vizinhos de v, ou NULL em caso de erro.
 */
vertice *vizinhanca(grafo g, vertice v)
{
	vertice *vizinhos = (vertice *) calloc (MIN_ALLOC, sizeof(vertice));
	long unsigned int tam_vizinhos = 0;
	long unsigned int alloc_vizinhos = MIN_ALLOC;

	if ( !vizinhos ) return NULL;

	for(aresta a = agfstedge(g->g, v); a; a = agnxtedge(g->g, a, v))
	{
		vertice head = aghead(a);

		if( head == v )
			vizinhos[tam_vizinhos++] = agtail(a);
		else
			vizinhos[tam_vizinhos++] = head;

		if( tam_vizinhos == alloc_vizinhos - 1 )
		{
			vizinhos = (vertice *) realloc (vizinhos, (alloc_vizinhos + MIN_ALLOC) * sizeof(vertice));

			if( !vizinhos ) return NULL;

			memset(vizinhos + alloc_vizinhos, 0, MIN_ALLOC * sizeof(vertice));
			alloc_vizinhos += MIN_ALLOC;
		}
	}

	return vizinhos;
}

/**
 * @brief      Retorna a diferença V1 - V2, onde V1,V2 são conjuntos de vertices
 *             Note que (V1 - V2) != (V2 - V1)
 *
 * @param      V1    Conjunto de vertices V1
 * @param      V2    Conjunto de vertices V2
 *
 * @return     Conjunto-diferença (V1 - V2)
 */
vertice *diferenca(vertice *V1, vertice *V2)
{
	vertice *diferenca = (vertice *) calloc (MIN_ALLOC, sizeof(vertice));
	long unsigned int tam_diferenca = 0;
	long unsigned int alloc_diferenca = MIN_ALLOC;

	if( !diferenca ) return NULL;

	for(long unsigned int i = 0; V1[i]; i++)
	{
		long unsigned int eh_da_intersecao = 0;

		for(long unsigned int j = 0; V2[j]; j++)
		{
			if( V1[i] == V2[j] )
			{
				eh_da_intersecao = 1;
				break;
			}
		}

		if( !eh_da_intersecao )
		{
			diferenca[tam_diferenca++] = V1[i];

			if( tam_diferenca == alloc_diferenca - 1 )
			{
				diferenca = (vertice *) realloc (diferenca, (alloc_diferenca + MIN_ALLOC) * sizeof(vertice));

				if( !diferenca ) return NULL;

				memset(diferenca + alloc_diferenca, 0, MIN_ALLOC * sizeof(vertice));
				alloc_diferenca += MIN_ALLOC;
			}
		}
	}

	return diferenca;
}

//------------------------------------------------------------------------------
// desaloca toda a memória usada em *g
// 
// devolve 1 em caso de sucesso,
//         ou 
//         0, caso contrário

int destroi_grafo(grafo g) {

	// Tenta liberar memoria se ponteiros nao forem nulos
	if(g && g->g)
	{	
		agclose(g->g);
		free(g);
		return 1;
	}
	else if(g)
	{
		free(g);
		return 1;
	}
	
	return 0;

}
//------------------------------------------------------------------------------
// lê um grafo no formato dot de input
// 
// devolve o grafo lido,
//         ou 
//         NULL, em caso de erro 

grafo le_grafo(FILE *input) {

	// Aloca memoria para uma estrutura
	grafo g = (grafo) malloc (sizeof(struct grafo));

	if( !g ) // Aborta se malloc falhou
		return NULL;

	// Le o grafo em DOT. 
	// O nome ja vem na estrutura Agraph_t,
	// e os vertices sao chamados c1, c2, ..., p1, p2, ...
	g->g = agread(input, NULL);

	// Se nao foi possivel ler o grafo do input, aborta
	if( !g->g )
		return NULL;

	return g;

}
//------------------------------------------------------------------------------
// escreve o grafo g em output usando o formato dot.
//
// devolve o grafo escrito,
//         ou 
//         NULL, em caso de erro 

grafo escreve_grafo(FILE *output, grafo g) {

	/*
	 * NOTA: Qualquer variacao de agwrite(Agraph_t *g, void *channel)
	 * com 'g' ou 'channel' invalidos gera SegFault
	 * 
	 * Se 'channel' eh valido mas eh arquivo de leitura, agwrite() retorna -1
	 */

	if( !output || !g || !g->g)
		return NULL;

	if(agwrite(g->g, output) < 0)
		return NULL;

	return g;

}
//------------------------------------------------------------------------------
// devolve o grafo de recomendações de g
//
// cada aresta {c,p} de H representa uma recomendação do produto p
// para o consumidor c, e tem um atributo "weight" que é um inteiro
// representando a intensidade da recomendação do produto p para o
// consumidor c.
//
// cada vértice de g tem um atributo "tipo" cujo valor é 'c' ou 'p',
// conforme o vértice seja consumidor ou produto, respectivamente

grafo recomendacoes(grafo g){

	/*
	 *  Validacao inicial e clonagem dos vertices do grafo g
	 */

	if( !g || !g->g ) return NULL;

	// Grafo de recomendacoes
	grafo r = (grafo) malloc (sizeof(struct grafo));

	if( !r ) return NULL;

	r->g = agopen(CONST_STR_RECOMENDACOES, Agstrictundirected, NULL);

	if( !r->g ) return NULL;

	agattr(r->g, AGEDGE, CONST_STR_WEIGHT, CONST_STR_0);
	agattr(r->g, AGNODE, CONST_STR_TIPO, CONST_STR_0);

	// Copia todos os vertices de G para R
	for(vertice v = agfstnode(g->g); v; v = agnxtnode(g->g, v))
	{
		vertice _v = agnode(r->g, agnameof(v), 1);
		agcopyattr(v, _v);
	}

	// Consumidores (o vetor sempre tem um indice a mais para marcar o fim)
	vertice *c = busca_atributo(g, CONST_STR_TIPO, "c");

	/*
	 * Computacao efetiva do grafo de recomendacoes
	 * O laço duplo passa por todos os vertices, dois a dois,
	 * sem repeticao (a,b == b,a)
	 */

	// Loop que passa pelos consumidores dois a dois
	// e gera as recomendacoes
	for(long unsigned int i = 0; c[i]; i++)
	{
		for(long unsigned int j = 0; j < i; j++)
			gera_recomendacoes(g, r, c[i], c[j]);

		for(long unsigned int j = i+1; c[j]; j++)
			gera_recomendacoes(g, r, c[i], c[j]);
	}

	// Loop que passa pelos consumidores
	// e atualiza os atributos 'weight'
	for(long unsigned int i = 0; c[i]; i++)
	{
		vertice v = agnode(r->g, agnameof(c[i]), 0);

		for(aresta a = agfstedge(r->g, v); a; a = agnxtedge(r->g, a, v))
		{
			// Todos os pesos com certeza ja existem em todas as arestas
			weight_t *w = (weight_t *) aggetrec (a, CONST_STR_WEIGHT_T, TRUE);
			
			char aux[500];
			sprintf(aux, "%lu", w->w);
			
			agset(a, CONST_STR_WEIGHT, aux);
		}
	}

	free(c);

	return r;
}

//------------------------------------------------------------------------------
