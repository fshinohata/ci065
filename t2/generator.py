import sys
import random

if len(sys.argv) != 2:
  print("Usage: python generator.py <n_vertexes>\n")
  exit()

try:
  n = int(sys.argv[1])
except ValueError as exception:
  print("ERROR: {} is not a number!\n".format(sys.argv[1]))
  exit()

print("strict graph bigguy {")

for i in range(n):
  rand = random.randint(0,n)
  for j in range(rand):
    otherEnd = random.randint(0,n-1)
    if otherEnd != i:
      print("\tv{} -- v{};".format(i, otherEnd))

print("}\n")
