#include <stdio.h>
#include <stdlib.h>
#include <graphviz/cgraph.h>
#include "grafo.h"

char CONST_STR_ROTULO[] = "rotulo";
char CONST_STR_COR[] = "cor";
char CONST_STR_COLOR[] = "color";
char CONST_STR_COR_0[] = "#000000";

/**
 * @brief Estrutura que representa um vertice
 * 
 */
struct vertice {
  Agnode_t *v;
};

/**
 * @brief Estrutura que representa o grafo
 * 
 */
struct grafo {
  Agraph_t *g;
  vertice *v;
};

/**
 * @brief Estrutura interna para armazenar os rotulos de um vertice
 * 
 */
typedef struct rotulo_t {
  Agrec_t hdr;
  long *rotulos;
  unsigned long tam;
  unsigned long tam_alocado;
} rotulo_t;

/**
 * @brief Estrutura interna que associa uma cor ao vertice
 * 
 */
typedef struct cor_t {
  Agrec_t hdr;
  unsigned long cor;
} cor_t;

/**
 * @brief Estrutura interna de lista genérica
 * 
 */
typedef struct lista_t {
  struct lista_t *anterior;
  struct lista_t *proximo;
  void *obj;
} lista_t;



/*
 * Funções auxiliares
 */

int lista_insere(lista_t **L, void *obj);
int lista_remove(lista_t **L, lista_t *item);
int lista_vazia(lista_t *L);

vertice retira_maior_rotulo(lista_t **L);
int adiciona_rotulo(rotulo_t *r, int n);

unsigned int gera_cor_unica(void);
char *cor_para_rgb(unsigned int cor);



/**
 * @brief Insere um objeto genérico na lista L
 * 
 * @param L      Lista L
 * @param obj    Objeto qualquer
 * @return int   1 em caso de sucesso, 0 caso contrário
 */
int lista_insere(lista_t **L, void *obj) {
  if ( L ) {
    lista_t *l = (lista_t *) malloc (sizeof(lista_t));

    if ( !l ) return 0;

    l->anterior = l;
    l->proximo = l;
    l->obj = obj;

    if ( !lista_vazia(*L) ) {
      l->anterior = (*L)->anterior;
      l->proximo = (*L);
      (*L)->anterior->proximo = l;
      (*L)->anterior = l;
    } else {
      (*L) = l;
    }

    return 1;
  }

  return 0;
}



/**
 * @brief Remove o 'item' da lista L. Supoe-se que o
 *        'item' pertence a L.
 * 
 * @param L      Lista L
 * @param item   Item da lista L
 * @return int   1 em caso de sucesso, 0 caso contrário.
 */
int lista_remove(lista_t **L, lista_t *item) {
  if ( L && !lista_vazia(*L) && item ) {
    item->anterior->proximo = item->proximo;
    item->proximo->anterior = item->anterior;
    
    if ( item == *L ) {
      if ( item->proximo == *L ) {
        *L = NULL;
      } else {
        *L = item->proximo;
      }
    }

    free(item);
    return 1;
  }

  return 0;
}



/**
 * @brief Verifica se a lista L está vazia.
 * 
 * @param L      Lista L
 * @return int   1 se estiver vazia, 0 caso contrário.
 */
int lista_vazia(lista_t *L) {
  return L == NULL ? 1 : 0;
}



/**
 * @brief Retorna o vertice de maior rotulo em v
 * 
 * @param v         (vertice *) conjunto de vertices
 * @param n         Tamanho de v
 * @return vertice  Vertice com maior rotulo em v
 */
vertice retira_maior_rotulo(lista_t **L) {
  if ( !lista_vazia(*L) ) {
    lista_t *percorredor = (*L);
    lista_t *item_a_remover = (*L);

    vertice v = (vertice) (*L)->obj;
    vertice vertice_de_maior_rotulo = v;

    rotulo_t *r = (rotulo_t *) aggetrec(v->v, CONST_STR_ROTULO, 0);
    rotulo_t *maior_rotulo = r;

    while ( percorredor->proximo != (*L) ) {
      v = (vertice) percorredor->proximo->obj;
      r = (rotulo_t *) aggetrec(v->v, CONST_STR_ROTULO, 0);

      /* Se r nao tem rotulos, nem adianta comparar */
      if ( r->rotulos ) {
        /* 
         * Se 'r' e 'maior_rotulo' tem rotulos,
         * Compara os valores em ordem lexicografica:
         *   [5,7,8] > [7,8] > [6,8] > [6,7] ...
         */
        if ( maior_rotulo->rotulos ) {
          int r_tem_mais_rotulos = (r->tam > maior_rotulo->tam);
          int r_tem_rotulo_maior = 0;
          int r_tem_rotulo_menor = 0;
          
          for(long i = (long) r->tam - 1, j = (long) maior_rotulo->tam - 1; (i >= 0) && (j >= 0); i--, j--) {
            if ( r->rotulos[i] > maior_rotulo->rotulos[j] ) {
              r_tem_rotulo_maior = 1;
              maior_rotulo = r;
              vertice_de_maior_rotulo = v;
              item_a_remover = percorredor->proximo;
              break;
            } else if ( r->rotulos[i] < maior_rotulo->rotulos[j] ) {
              r_tem_rotulo_menor = 1;
              break;
            }
          }

          if ( !r_tem_rotulo_maior && !r_tem_rotulo_menor && r_tem_mais_rotulos )  {
            maior_rotulo = r;
            vertice_de_maior_rotulo = v;
            item_a_remover = percorredor->proximo;
          }

        } else {
          /* 
           * Se cair nesse 'if', então
           * maior_rotulo->rotulos == NULL,
           * i.e. o 'vertice de maior rotulo' nao tem rotulos
           * mas 'r' tem, então o maior_rotulo é 'r', sem sombra de
           * dúvidas.
           */
          maior_rotulo = r;
          vertice_de_maior_rotulo = v;
          item_a_remover = percorredor->proximo;
        }
      }

      percorredor = percorredor->proximo;
    }

    lista_remove(L, item_a_remover);

    return vertice_de_maior_rotulo;
  }
  return NULL;
}



/**
 * @brief Adiciona um novo rotulo 'n' em 'r'. 
 *        Alocações de rótulos são realizadas dentro desta função.
 * 
 * @param r      struct rotulo pointer
 * @param n      rotulo a associar a r
 * @return int   0 em caso de sucesso, -1 em caso de erro.
 */
int adiciona_rotulo(rotulo_t *r, int n) {
  static unsigned long tam_alocacao_inicial = 16;

  if ( r ) {
    if ( !r->rotulos ) {
      r->rotulos = (long *) malloc (tam_alocacao_inicial * sizeof(long));

      if ( !r->rotulos ) return -1;

      r->tam = 0;
      r->tam_alocado = tam_alocacao_inicial;
    }

    unsigned long i = r->tam;

    while ( i > 0 && n < r->rotulos[i - 1] ) {
      r->rotulos[i] = r->rotulos[i - 1];
      i--;
    }

    r->rotulos[i] = n;
    (r->tam)++;

    if ( r->tam == r->tam_alocado ) {
      r->tam_alocado <<= 1;
      r->rotulos = (long *) realloc (r->rotulos, (r->tam_alocado) * sizeof(long));
      
      if ( !r->rotulos ) return -1;
    }
  }

  return 0;
}



/**
 * @brief Gera e devolve uma cor única em um (unsigned int) 
 *        no formato 0x00rrggbb. Algumas cores são padrão.
 *        Se todas as cores padrão forem utilizadas, cores 'dinâmicas'
 *        passarão a ser geradas. Existe um fator probabilístico de
 *        a função NUNCA conseguir gerar uma cor única nova.
 * 
 * @return unsigned int   Nova cor no formato 0x00rrggbb
 */
unsigned int gera_cor_unica(void) {
  static unsigned int cores_padrao[] = {
    0x00FF0000,
    0x0000FF00,
    0x000000FF,
    0x00C89664,
    0x006496C8,
    0x0032FF00
  };
  static unsigned int n_cores_padrao = 6;
  static unsigned int n_cores_padrao_usadas = 0;
  static unsigned int *cores_dinamicas = NULL;
  static unsigned int n_cores_dinamicas = 0;
  static unsigned int tam_alocacao_inicial = 10;
  static unsigned int tam_alocado = 10;

  if ( n_cores_padrao_usadas == n_cores_padrao ) {
    if ( !cores_dinamicas ) {
      cores_dinamicas = (unsigned int *) malloc (tam_alocacao_inicial * sizeof(unsigned int));
    }
    
    int cor_unica;
    unsigned int nova_cor;

    do {
      unsigned char R = (unsigned char) rand();
      unsigned char G = (unsigned char) rand();
      unsigned char B = (unsigned char) rand();

      nova_cor = 0x00FFFFFF & ( (R << 16) | (G << 8) | B );

      cor_unica = 1;

      for (unsigned int k = 0; k < n_cores_padrao; k++) {
        if (cores_padrao[k] == nova_cor) {
          cor_unica = 0;
          break;
        }
      }

      if ( cor_unica ) {
        for(unsigned int k = 0; k < n_cores_dinamicas; k++) {
          if ( cores_dinamicas[k] == nova_cor ) {
            cor_unica = 0;
            break;
          }
        }
      }
    } while ( !cor_unica );

    cores_dinamicas[n_cores_dinamicas] = nova_cor;

    if ( n_cores_dinamicas + 1 == tam_alocado ) {
      tam_alocado <<= 1;
      cores_dinamicas = (unsigned int *) realloc (cores_dinamicas, tam_alocado * sizeof(unsigned int));
    }

    return cores_dinamicas[n_cores_dinamicas++];
  } else {
    return cores_padrao[n_cores_padrao_usadas++];
  }
}



/**
 * @brief Converte a cor em unsigned int (4 bytes) recebida
 *        para uma string no formato "#rrggbb".
 * 
 *        Note que a conversão ignora o valor do byte mais
 *        significativo do unsigned int.
 * 
 * @param cor      Cor para converter
 * @return char*   String temporária com a cor formatada 
 *                 em "#rrggbb"
 */
char *cor_para_rgb(unsigned int cor) {
  static char aux[255];

  unsigned char R = 0x000000FF & (cor >> 16);
  unsigned char G = 0x000000FF & (cor >> 8);
  unsigned char B = 0x000000FF & (cor);

  sprintf(aux, "#%2x%2x%2x", R, G, B);

  for(int i = 1; aux[i]; i++) {
    if ( aux[i] == ' ' ) aux[i] = '0';
  }

  return aux;
}



/*
 * Funções de grafo.h
 */

/**
 * @brief Desaloca toda a memória usada em *g
 * 
 * @param g      Grafo g
 * @return int   1 em caso de sucesso, ou 0, caso contrário
 */
int destroi_grafo(grafo g) {
  int status = 1;
  if ( g ) {
    if ( g->g ) {
      /* Desaloca todos os rotulos */
      long n = n_vertices(g);
      rotulo_t *r;

      for(long i = 0; i < n; i++) {
        r = (rotulo_t *) aggetrec(g->v[i]->v, CONST_STR_ROTULO, 0);
        if ( r->rotulos ) free(r->rotulos);
        free(g->v[i]);
      }

      free(g->v);

      /* 0 de agclose => sucesso */
      status = agclose(g->g) == 0 ? 1 : 0;
    }
    free(g);
  }
  return status;
}



/**
 * @brief Devolve o número de vértices de g
 * 
 * @param g      Grafo g
 * @return int   Número de vértices do grafo (retorna 0 se não estiver alocado)
 */
int n_vertices(grafo g) {
  if ( g && g->g ) {
    return agnnodes(g->g);
  }
  return 0;
}



/**
 * @brief Devolve o vértice de nome 'nome' em g
 * 
 * @param nome        String com o nome do vertice
 * @param g           Grafo g
 * @return vertice    Vertice com o nome 'nome', ou NULL se não houver.
 */
vertice vertice_de_nome(char *nome, grafo g) {
  if ( g && g->g ) {
    Agnode_t *v = agnode(g->g, nome, 0);

    int n = n_vertices(g);

    for(int i = 0; i < n; i++) {
      if ( g->v[i]->v == v ) {
        return g->v[i];
      }
    }
  }
  return NULL;
}



/**
 * @brief Lê um grafo no formato dot de input e o converte para um struct grafo
 * 
 * @param input     (FILE *) com o arquivo de entrada (aberto para leitura)
 * @return grafo    Grafo que reflete o input, ou NULL em caso de erro
 */
grafo le_grafo(FILE *input) {
  if( input ) {
    grafo g = (grafo) malloc (sizeof(struct grafo));

    if( g ) {
      g->g = agread(input, NULL);

      if( g->g ) {
        int n = agnnodes(g->g);
        unsigned long ln = (unsigned long) n;
        unsigned long i = 0;
        g->v = (vertice *) malloc (ln * sizeof(vertice));

        if ( !g->v ) {
          agclose(g->g);
          free(g);
          return NULL;
        }

        for(Agnode_t *v = agfstnode(g->g); v; v = agnxtnode(g->g, v), i++) {
          g->v[i] = (vertice) malloc (sizeof(struct vertice));

          if ( !g->v[i] ) {
            free(g->v);
            agclose(g->g);
            free(g);
            return NULL;
          }

          g->v[i]->v = v;
          cor_t *c = (cor_t *) agbindrec(g->v[i]->v, CONST_STR_COR, sizeof(cor_t), 0);
          c->cor = 0;
        }

        return g;
      } else {
        free(g);
        return NULL;
      }
    }
  }
  return NULL;
}



/**
 * @brief escreve o grafo g em output usando o formato dot.
 *        cada vértice do grafo escrito tem um atributo de nome "color" e o
 *        valor desse atributo deve ser uma cor especificada no formato
 *        "#rrggbb" onde rr, gg e bb são números de dois dígitos em
 *        representação hexadecimal indicando as intensidades de vermelho
 *        (red), verde (green) e azul (blue) em que se decompõe essa cor
 * 
 *        o valor deste atributo para o vértice v deve ser uma função do
 *        valor de cor(v,g)
 * 
 * @param output   FILE * para arquivo de saida
 * @param g        Grafo g
 * @return grafo   Grafo escrito, ou NULL em caso de erro.
 */

grafo escreve_grafo(FILE *output, grafo g) {
  if ( output && g && g->g ) {
    agwrite(g->g, output);
    return g;
  }
  return NULL;
}



/**
 * @brief Retorna a cor do vértice v se ele pertencer ao grafo g
 * 
 * @param v               Vertice de g
 * @param g               Grafo g
 * @return unsigned int   Cor de v
 */
unsigned int cor(vertice v, grafo g) {
  if ( g && g->g && v && v->v && agnode(g->g, agnameof(v->v), 0)) {
    cor_t *c = (cor_t *) aggetrec(v->v, CONST_STR_COR, 0);
    return (unsigned int) c->cor;
  }
  return 0;
}



/**
 * @brief Preenche o vetor v (presumidamente um vetor com n_vertices(g)
 *        posições) com os vértices de g ordenados de acordo com uma busca em
 *        largura lexicográfica sobre g a partir de r e devolve v
 * 
 * @param r            Vertice "raiz" de g
 * @param g            Grafo g
 * @param v            Vetor de (vertice) com n_vertices(g) elementos alocados
 * @return vertice*    Vetor v propriamente preenchido
 */
vertice *busca_lexicografica(vertice r, grafo g, vertice *v) {
  int n = n_vertices(g);
  lista_t *L = NULL;
  rotulo_t *rt;

  /* 
   * L <- Lista vazia
   * Para cada v em V(G), 
   *  rotulo(v) <- NULL
   *  insira v em L
   */
  for(Agnode_t *_v = agfstnode(g->g); _v; _v = agnxtnode(g->g, _v)) {
    rt = (rotulo_t *) agbindrec(_v, CONST_STR_ROTULO, sizeof(rotulo_t), 0);
    rt->rotulos = NULL;
    rt->tam = 0;
    
    vertice __v = (vertice) malloc (sizeof(struct vertice));
    __v->v = _v;
    lista_insere(&L, __v);
  }

  /* 
   * rotulo(rt) <- {n}
   * v[0] <- r
   */
  rt = (rotulo_t *) aggetrec(r->v, CONST_STR_ROTULO, 0);
  adiciona_rotulo(rt, n);
  
  unsigned long i = 0;

  
  /*
   * n <- |L|
   * Enquanto L não está vazia,
   *   retire o vertice v de L de maior rotulo
   *   n <- |L|
   *   Para cada vizinho vz de v
   *     adicione n ao rotulo de vz
   */
  while ( !lista_vazia(L) ) {
    vertice _v = retira_maior_rotulo(&L);
    n--;
    
    for(Agedge_t *e = agfstedge(g->g, _v->v); e; e = agnxtedge(g->g, e, _v->v)) {
      Agnode_t *vz = aghead(e);

      if (vz == _v->v) vz = agtail(e);

      rt = (rotulo_t *) aggetrec(vz, CONST_STR_ROTULO, 0);
      adiciona_rotulo(rt, n);
    }

    v[i] = _v;
    i++;
  }

  return v;
}



/**
 * @brief colore os vértices de g de maneira "gulosa" segundo a ordem dos
 *        vértices em v e devolve o número de cores utilizado
 * 
 *        ao final da execução,
 *            1. cor(v,g) > 0 para todo vértice de g
 *            2. cor(u,g) != cor(v,g), para toda aresta {u,v} de g
 * 
 * @param g               Grafo g
 * @param v               Vetor de vertices v
 * @return unsigned int   Número de cores utilizadas para colorir g
 */
unsigned int colore(grafo g, vertice *v) {
  if ( !g || !g->g || !v ) return 0;

 
  int n = n_vertices(g);
  unsigned int tam_alocacao_inicial = 10;
  unsigned int tam_alocado = 10;
  unsigned int *cores = (unsigned int *) malloc (tam_alocacao_inicial * sizeof(unsigned int));
  unsigned int n_cores = 0;

  if ( !cores ) {
    fprintf(stderr, "ERRO: Não foi possível colorir o grafo.\n");
    exit(1);
  }

  agattr(g->g, AGNODE, CONST_STR_COLOR, CONST_STR_COR_0);

  /*
   * Para cada vertice _v de v percorrido em ordem reversa
   *   Se uma ou mais cores ja foram utilizadas
   *     cor <- primeira cor ja utilizada
   *   Senao
   *     cor <- nova cor
   * 
   *   Se ja existe um vizinho de _v com cor 'cor'
   *     cor <- nova cor
   * 
   *   pinta(_v, cor)
   */

  for(int i = n-1; i >= 0; i--) {

    if ( n_cores == 0 ) {
      cores[0] = gera_cor_unica();
      n_cores++;
    }

    int cor_utilizada;
    unsigned int cor;
    
    for(unsigned int k = 0; k < n_cores; k++) {
      
      cor_utilizada = 0;
      
      for(Agedge_t *e = agfstedge(g->g, v[i]->v); e; e = agnxtedge(g->g, e, v[i]->v)) {
        Agnode_t *_v = aghead(e);

        if ( _v == v[i]->v ) _v = agtail(e);

        cor_t *c = (cor_t *) aggetrec(_v, CONST_STR_COR, 0);
        
        if ( c->cor == cores[k] ) {
          cor_utilizada = 1;
          break;
        }
      }

      if ( !cor_utilizada ) {
        cor = cores[k];
        break;
      }
    }

    if ( cor_utilizada ) {
      cores[n_cores] = cor = gera_cor_unica();
      n_cores++;
      
      if ( n_cores == tam_alocado ) {
        tam_alocado <<= 1;
        cores = (unsigned int *) realloc (cores, tam_alocado * sizeof(unsigned int));
      }
    }

    cor_t *c = (cor_t *) aggetrec(v[i]->v, CONST_STR_COR, 0);
    c->cor = cor;
    agsafeset(v[i]->v, CONST_STR_COLOR, cor_para_rgb(cor), CONST_STR_COR_0);
  }

  free(cores);

  return n_cores;
}