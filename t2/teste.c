#include <stdio.h>
#include <stdlib.h>
#include "grafo.h"

char CONST_STR_A[] = "BARREIRAS";

int main(int argc, char *argv[]) {

  if ( argc != 2 ) {
    fprintf(stderr, "Como usar: ./teste vertice_inicial < arquivo\n");
    exit(1);
  }

  grafo g = le_grafo(stdin);

  vertice r = vertice_de_nome(argv[1], g);
  
  if ( !r ) {
    fprintf(stderr, "Vertice \"%s\" não existe!\n", argv[1]);
    exit(1);
  }

  int n = n_vertices(g);

  vertice *v = (vertice *) malloc ((unsigned long) n * sizeof(vertice));
  v = busca_lexicografica(r, g, v);

  printf("Cores utilizadas para colorir: %d\n", colore(g, v));

  escreve_grafo(stderr, g);

  for(int i = 0; i < n; i++) {
    if ( v[i] ) free(v[i]);
  }

  free(v);
  return 0;
}